
%load RESULTTABLE.mat RESULTS
load stat0123all.mat

ALLSPKS={'andy','drucie','rita','robin'};
NUMSPKS=length(ALLSPKS);
WS=21;

PPP=[]; QQQ=[];
%fid=fopen('tmp','w');
for numfold=1:3
  %string=[num2str(numfold) '0\\%% & %03.1f & %03.1f & %03.1f & %03.1f & %03.1f & %03.1f & %03.1f & %03.1f \\\\ \\hline \n'];
  
  EE1=[]; EE2=[]; EE3=[]; EE4=[]; EE5=[]; EE6=[]; EE7=[]; EE8=[]; EE4fa=[]; EE4gt=[]; EE8fa=[]; EE8gt=[];
  for testspeaker=1:NUMSPKS
    SS=RESULTS{testspeaker,numfold};
    EE1=[EE1, 100*SS(1,1)];
    EE2=[EE2, 100*SS(2,1)];
    EE3=[EE3, 100*SS(3,1)];
    EE4=[EE4, 100*SS(4,1)];
    EE4fa=[EE4fa, 100*FA{numfold}(testspeaker,1)];
    EE4gt=[EE4gt, 100*GT{numfold}(testspeaker,1)];
    
    EE5=[EE5, 100*mean(SS(1,2:end))];   % No Adaptation.
    EE6=[EE6, 100*mean(SS(2,2:end))];   % Forced-alignment adaptation.
    EE7=[EE7, 100*mean(SS(3,2:end))];   % Ground-truth adaptation.
    EE8=[EE8, 100*mean(SS(4,2:end))];     % Old adaptation.
    EE8fa=[EE8fa, 100*FA{numfold}(testspeaker,2:end)];
    EE8gt=[EE8gt, 100*GT{numfold}(testspeaker,2:end)];
    
  end
%  fprintf(fid,string,mean(EE1),mean(EE2),mean(EE3),mean(EE4),mean(EE5),mean(EE6),mean(EE7),mean(EE8),mean);
  PPP=[PPP; mean(EE1),mean(EE2),mean(EE4fa),mean(EE3),mean(EE4),mean(EE4gt),mean(EE5),mean(EE6),mean(EE8fa),mean(EE7),mean(EE8),mean(EE8gt)];
  QQQ=[QQQ; std(EE1),std(EE2),std(EE4fa),std(EE3),std(EE4),std(EE4gt),std(EE5),std(EE6),std(EE8fa),std(EE7),std(EE8),std(EE8gt)];
end
%fclose(fid);

figure(1); clf; hold on; set(gca,'fontsize',16);
COLORS={'y','b','m','r','g','m','y','b','m','r','g','m'};
j=1; h1=bar(0.9, PPP(1,j),COLORS{j}); set(h1,'BarWidth', get(h1,'BarWidth')/3); errorbar(get(h1,'XData'), get(h1,'YData'), QQQ(1,j),['k.'],'linewidth',2);

hs=bar(1.3, mean([73.4 65.0 68.9 57.5]), 'c'); set(hs,'BarWidth', get(hs,'BarWidth')/3); 
errorbar(get(hs,'XData'), get(hs,'YData'), std([73.4 65.0 68.9 57.5]), 'k.','linewidth',2);

% % % hn=bar(1.3, mean([82.72 81.36 76.80 76.33]), 'k'); set(hn,'BarWidth', get(hn,'BarWidth')/3); 
% % % errorbar(get(hn,'XData'), get(hn,'YData'), std([82.72 81.36 76.80 76.33]), 'k.','linewidth',2);

j=2; h2=bar(j-0.3:0.3:j+0.3, PPP(:,j),COLORS{j}); errorbar(get(h2,'XData'), get(h2,'YData'), QQQ(:,j),['k.'],'linewidth',2);
j=3; h3=bar(j-0.3:0.3:j+0.3, PPP(:,j),'FaceColor',[1 0.5 0]); errorbar(get(h3,'XData'), get(h3,'YData'), QQQ(:,j),['k.'],'linewidth',2);
j=4; h4=bar(j-0.3:0.3:j+0.3, PPP(:,j),COLORS{j}); errorbar(get(h4,'XData'), get(h4,'YData'), QQQ(:,j),['k.'],'linewidth',2);
j=5; h5=bar(j-0.3:0.3:j+0.3, PPP(:,j),COLORS{j}); errorbar(get(h5,'XData'), get(h5,'YData'), QQQ(:,j),['k.'],'linewidth',2);
j=6; h6=bar(j-0.3:0.3:j+0.3, PPP(:,j),COLORS{j}); errorbar(get(h6,'XData'), get(h6,'YData'), QQQ(:,j),['k.'],'linewidth',2);

j=7; h7=bar(j+0.3, PPP(1,j),COLORS{j}); set(h7,'BarWidth', get(h7,'BarWidth')/3); errorbar(get(h7,'XData'), get(h7,'YData'), QQQ(1,j),['k.'],'linewidth',2);
j=8; h8=bar(j-0.3:0.3:j+0.3, PPP(:,j),COLORS{j}); errorbar(get(h8,'XData'), get(h8,'YData'), QQQ(:,j),['k.'],'linewidth',2);
j=9; h9=bar(j-0.3:0.3:j+0.3, PPP(:,j),'FaceColor',[1 0.5 0]); errorbar(get(h9,'XData'), get(h9,'YData'), QQQ(:,j),['k.'],'linewidth',2);
j=10; h10=bar(j-0.3:0.3:j+0.3, PPP(:,j),COLORS{j}); errorbar(get(h10,'XData'), get(h10,'YData'), QQQ(:,j),['k.'],'linewidth',2);
j=11; h11=bar(j-0.3:0.3:j+0.3, PPP(:,j),COLORS{j}); errorbar(get(h11,'XData'), get(h11,'YData'), QQQ(:,j),['k.'],'linewidth',2);
j=12; h12=bar(j-0.3:0.3:j+0.3, PPP(:,j),COLORS{j}); errorbar(get(h12,'XData'), get(h12,'YData'), QQQ(:,j),['k.'],'linewidth',2);

axis([0.4 12.7 57 99]);  ylabel('Accuracy (%)');
box on;
legend([h1,hs,h2,h3,h4,h5,h6],'No Adapt.','Speed Norm.','LIN+UP FA','FT FA','LIN+UP GT','LIN+LON GT','FT GT',...
  'location',[0.135 0.55 0.2 0.38]);
  %'location','northwest','orientation','horizontal');
legend boxoff;
set(gca,'xtick',[get(h1,'XData'), get(hs,'XData'),  get(h2,'XData'), get(h3,'XData'), get(h4,'XData'), ...
  get(h5,'XData'), get(h6,'XData'), get(h7,'XData'), get(h8,'XData'), get(h9,'XData'), get(h10,'XData'), get(h11,'XData'), get(h12,'XData')], ...
  'xticklabel', {'0', '0', '1','2','3','1','2','3', '1','2','3', '1','2','3','1','2','3',...
  '0', '1','2','3','1','2','3', '1','2','3','1','2','3','1','2','3'});
text(4.0,90,'letter DNN','fontsize',18);   text(9.0,94.5,'phon. feature DNN','fontsize',18);
print -depsc adapt_bar2.eps


% % % for numfold=3
% % %   fid=fopen('tmp','w');
% % %   fprintf(fid,'\\begin{tabular}{@{}|c||c|c|c|c|c|c|c|c|@{}} \\hline \n');
% % %   fprintf(fid,['& \\multicolumn{2}{|c|}{No adapt.} & \\multicolumn{2}{|c|}{Forced-align.} '...
% % %     '& \\multicolumn{2}{|c|}{Ground-truth} & \\multicolumn{2}{|c|}{Old adapt.} \\\\ \\hline \n']);
% % %   fprintf(fid,['& Letter & Phono. & Letter & Phono. & Letter & Phono. & Letter & Phono. \\\\ \\hline \n']);
% % %   for testspeaker=1:NUMSPKS
% % %     EE=RESULTS{testspeaker,numfold}
% % %
% % %     fprintf(fid,'%s & ', ALLSPKS{testspeaker});
% % %     string='%03.1f & %03.1f & %03.1f & %03.1f & %03.1f & %03.1f & %03.1f & %03.1f \\\\ \\hline \n';
% % %     fprintf(fid, string, ...
% % %       100*EE(1,1), 100*mean(EE(1,2:end)), ...  % No Adaptation.
% % %       100*EE(2,1), 100*mean(EE(2,2:end)), ...  % Forced-alignment adaptation.
% % %       100*EE(3,1), 100*mean(EE(3,2:end)), ...  % Ground-truth adaptation.
% % %       100*EE(4,1), 100*mean(EE(4,2:end)));     % Old adaptation.
% % %   end
% % %   fprintf(fid,'\\end{tabular}\n');
% % %   fclose(fid);
% % % end