filename=main

$(filename).pdf: $(filename).tex intro.tex method.tex dnn.tex experiments.tex conclusions.tex icassp16b.bib reference.bib
	pdflatex $(filename)
	bibtex --min-crossrefs=20 $(filename)
	pdflatex $(filename)
	pdflatex $(filename)

clean:
	rm -f $(filename)*.dvi
	rm -f $(filename)*.ps
	rm -f $(filename)*.pdf
	rm -f $(filename)*.bbl
	rm -f $(filename)*.aux
	rm -f $(filename)*.log

